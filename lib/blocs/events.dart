abstract class CityEvent {}

class FetchData extends CityEvent {}

class InputData extends CityEvent {
  String input;
  InputData({this.input});
}

class Choice extends CityEvent {
  final String choice;
  Choice({this.choice});
}
