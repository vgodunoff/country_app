import 'package:country_app/models/city_model.dart';
import 'package:flutter/cupertino.dart';

abstract class CityState {}

class Loading extends CityState {
  final bool isLoading;

  Loading(this.isLoading);
}

class Loaded extends CityState {
  final List<Destination> listOfCityList;
  Loaded({@required this.listOfCityList});
}

class Error extends CityState {}

class Searched extends CityState {
  final List<Destination> listOfCitySearched;
  Searched({this.listOfCitySearched});
}

class HomePage extends CityState {
  final String cityChosen;

  HomePage({this.cityChosen});
}
