import 'package:country_app/blocs/bloc.dart';
import 'package:country_app/blocs/events.dart';
import 'package:country_app/blocs/states.dart';
import 'package:flutter/material.dart';
import 'package:country_app/models/city_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // создаем переменную в которую записываем ссылку на объект блока,
    // который мы передали на странице MyHomePage
    // в конструкторе BlocProvider.value(value: bloc, child: SearchPage())
    // и сразу добавляем в поток ивент FetchData()
    //чтобы загрузить данные из API для отображения всех городов

    final bloc = BlocProvider.of<CityBloc>(context)..add(FetchData());
    return Scaffold(
      backgroundColor: Color(0x919397),
      body: BlocConsumer<CityBloc, CityState>(
          // в listener рекомендуется использовать SnackBar , showDialog
          listener: (context, state) async {
            //состояние Loading имеет boolean поле isLoading
            //если оно true то рисуем значок загрузки CircularProgressIndicator()
            // если false выходим из showDialog, то есть убираем значок загрузки
            if (state is Loading) {
              if (state.isLoading) {
                showDialog(
                    context: context,
                    builder: (context) {
                      return Center(child: CircularProgressIndicator());
                    });
              } else {
                Navigator.pop(context);
              }
            }
          },
          //перерисовываем страницу если состоянием является Loaded или Searched
          buildWhen: (previous, current) =>
              (current is Loaded || current is Searched),
          builder: (context, state) {
            if (state is Loaded) {
              //если состояние Loaded рисуем весь список городов
              //в AppBar есть место для поиска, где можно ввести название города
              //когда пользователь начинает вводить город, то в поток добавляется ивент
              //InputData(input: value) и на выходе из потока получаем состояние Searched
              return CustomScrollView(
                slivers: [
                  SliverAppBar(
                    title: TextField(
                      onChanged: (value) async {
                        BlocProvider.of<CityBloc>(context)
                            .add(InputData(input: value));
                      },
                      enabled: true,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.fromLTRB(20, 4, 20, 4),
                        hintText: 'поиск',
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                    centerTitle: true,
                    backgroundColor: Color(0xff3C3E44),
                    expandedHeight: 60,
                    pinned: true,
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      var _destination;
                      if (state.listOfCityList[index] is Country) {
                        _destination = state.listOfCityList[index] as Country;
                        return buildCard(context, true, _destination.name);
                      } else {
                        _destination = state.listOfCityList[index] as City;
                        return buildCard(context, false, _destination.name);
                      }
                    }, childCount: state.listOfCityList.length),
                  ),
                ],
              );
            }
            if (state is Searched) {
              // когда состояние Searched мы отображаем результаты фильтра
              // то есть только те города (вместе со страной к которой принадлежит город)
              // или страну название которых соотвествует вводимым буквам
              return CustomScrollView(
                slivers: [
                  SliverAppBar(
                    title: TextField(
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          BlocProvider.of<CityBloc>(context)
                              .add(InputData(input: value));
                        }
                      },
                      enabled: true,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.fromLTRB(20, 4, 20, 4),
                        hintText: 'поиск',
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                    centerTitle: true,
                    backgroundColor: Color(0xff3C3E44),
                    expandedHeight: 60,
                    pinned: true,
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      var _destination;
                      if (state.listOfCitySearched[index] is Country) {
                        _destination =
                            state.listOfCitySearched[index] as Country;
                        return buildCard(context, true, _destination.name);
                      } else {
                        _destination = state.listOfCitySearched[index] as City;
                        return buildCard(context, false, _destination.name);
                      }
                    }, childCount: state.listOfCitySearched.length),
                  ),
                ],
              );
            }
            return const SizedBox.shrink();
          }),
    );
  }

  InkWell buildCard(
      BuildContext context, bool isCountry, String nameOfCityOrCountry) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, nameOfCityOrCountry);
      },
      child: Card(
        color: Color(0xff3C3E44),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 9.82),
        child: ListTile(
          contentPadding: EdgeInsets.all(16),
          leading: Container(
            width: 60,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isCountry ? Color(0xffC4C4C4) : null),
          ),
          title: isCountry
              ? Text(
                  'Страна $nameOfCityOrCountry',
                  style: TextStyle(
                    color: Color(0xffFFFFFF),
                    fontSize: 25,
                  ),
                )
              : Text(
                  'Город $nameOfCityOrCountry',
                  style: TextStyle(
                    color: Color(0xffFFFFFF),
                    fontSize: 25,
                  ),
                ),
        ),
      ),
    );
  }
}
