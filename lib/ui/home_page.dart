import 'package:country_app/blocs/bloc.dart';
import 'package:country_app/blocs/events.dart';
import 'package:country_app/blocs/states.dart';
import 'package:country_app/ui/search_city.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CityBloc>(context);

    return Scaffold(
      backgroundColor: Color(0xff919397),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Container(
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.all(1),
              width: double.infinity,
              child: TextButton(
                style: TextButton.styleFrom(
                    textStyle: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                    // minimumSize: Size(303, 53),
                    backgroundColor: Colors.red,
                    primary: Colors.black),
                onPressed: () async {
                  // По нажатию на кнопку переходим на страницу поиска городов,
                  //используем конструктор BlocProvider.value чтобы использовать один и тот же bloc,
                  // по рецепту на сайте разработчиков пакета flutter_bloc
                  // https://bloclibrary.dev/#/recipesflutterblocaccess?id=named-route-access
                  //It is critical that we are using BlocProvider's value constructor in this case
                  // because we are providing an existing instance of CounterBloc.
                  // The value constructor of BlocProvider should be used only in cases where we want
                  // to provide an existing bloc to a new subtree.
                  final city = await Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return BlocProvider.value(value: bloc, child: SearchPage());
                  }));

                  //по возращению из страницы поиска получаем город (city) и запускаем ивент Choice
                  //Ивент Choice в блоке трансформируется в состояние HomePage, в который передаем city
                  bloc.add(Choice(choice: city));
                },
                // если состояние HomePage, то на кнопке будет написан город,
                // который мы получили на странице поиска
                // иначе на кнопке будет написано 'Выберите пункт назначения'
                child: BlocBuilder<CityBloc, CityState>(
                  builder: (context, state) {
                    if (state is HomePage) {
                      return Text(state.cityChosen);
                    }
                    return Text('Выберите пункт назначения');
                  },
                ),
              ),
            ),
            centerTitle: true,
            backgroundColor: Color(0xff3C3E44),
            expandedHeight: 150,
            actions: [],
          ),
        ],
      ),
    );
  }
}
