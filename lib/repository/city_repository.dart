import 'dart:convert';
import 'package:country_app/models/city_model.dart';
import 'package:http/http.dart' as http;
import 'package:country_app/models/country_city_model.dart';

class FetchDataFromApi {
  Future<List<dynamic>> fetchData() async {
    final http.Response response =
        await http.get(Uri.parse("https://api.hh.ru/areas"));
    if (response.statusCode == 200) {
      List<dynamic> decodedJson = json.decode(response.body) as List;
      return decodedJson;
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<List<Destination>> cityFromJsonDecoded() async {
    List<dynamic> countries = await fetchData(); //download by api
    List<Destination> _listOfCityAndCountry = [];
    String _country;

    for (int i = 0; i < countries.length; i++) {
      //работаем с каждой страной в полученном списке
      if (countries[i]['parentId'] == null) {
        _country = countries[i]['name'];
        _listOfCityAndCountry.add(Country(name: _country));
      }
      //заходим в лист areas страны
      for (int j = 0; j < countries[i]['areas'].length; j++) {
        //если в объекте нашего списка есть промежуточная Республика
        if (countries[i]['areas'][j]['areas'].isNotEmpty) {
          //работаем с полем areas Республики или области
          for (int a = 0; a < countries[i]['areas'][j]['areas'].length; a++) {
            if (countries[i]['areas'][j]['areas'][a]['areas'].isEmpty) {
              //если у этого объекта поле areas пустое, то этот объект Город
              // и добавляем его в список , как город
              _listOfCityAndCountry.add(
                City(name: countries[i]['areas'][j]['areas'][a]['name']),
              );
            }
          }
        } else if (countries[i]['areas'][j]['areas'].isEmpty) {
          //или берем город и добавляем в список
          _listOfCityAndCountry.add(
            City(name: countries[i]['areas'][j]['name']),
          );
        }
      }
    }

    return _listOfCityAndCountry;
  }

  // method for searching city matching first symbols of city names
  List<Destination> searchCity(String input, List<Destination> destinations) {
    List<Destination> listCity = [];
    List<Destination> tmp = [];
    int k;
    for (int i = 0; i < destinations.length; i++) {
      if (destinations[i] is Country && tmp.isNotEmpty) {
        // если начинается новая страна со своими городами
        // то добавлем временный список в фильтрованный список

        // проверить включена ли страна
        if (tmp[0] is Country) {
          listCity.addAll(tmp);
          //и зачищаем врем. список
          tmp.clear();
        } else {
          // если во временном списке добавлены только города,
          // в начало списка добавляем соответствующую страну
          //используем индекс, который мы зафиксировали в переменной k
          tmp.insert(0, destinations[k]);
          listCity.addAll(tmp);
          tmp.clear();
        }
      }

      if (destinations[i] is Country) {
        //перебираем все объекты списка
        //если объект являетя страной, то запоминаем его индекс в списке
        k = i;
        if (destinations[i].name.startsWith(input)) {
          //если первые буквы страны соответсвтуют вводимым символам
          // то добавлеям эту страну во временный список
          tmp.add(destinations[i]);
          continue;
        }
      } else {
        if (destinations[i].name.startsWith(input)) {
          //если объектом является город, то также проверяем на соответствие вводимым символам
          // и добавляем
          tmp.add(destinations[i]);
        }
      }

      // добавлем временный список в фильтрованный список также если подошли к концу списка
      if (i == destinations.length - 1 && tmp.isNotEmpty) {
        if (tmp[0] is Country) {
          listCity.addAll(tmp);
          tmp.clear();
        } else {
          tmp.insert(0, destinations[k]);
          listCity.addAll(tmp);
          tmp.clear();
        }
      }
    }

    return listCity;
  }
}
