abstract class Destination {
  String name;
}

class Country extends Destination {
  @override
  String name;

  Country({this.name});
}

class City extends Destination {
  String name;

  City({this.name});
}
