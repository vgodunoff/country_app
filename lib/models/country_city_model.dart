// To parse this JSON data, do
//
//     final city = cityFromJson(jsonString);

import 'dart:convert';

List<CountryModel> cityFromJson(String str) => List<CountryModel>.from(
    json.decode(str).map((x) => CountryModel.fromJson(x)));

class CountryModel {
  CountryModel({
    this.id,
    this.parentId,
    this.name,
    this.areas,
  });

  String id;
  String parentId;
  String name;
  List<CountryModel> areas;

  factory CountryModel.fromJson(Map<String, dynamic> json) => CountryModel(
        id: json["id"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        name: json["name"],
        areas: List<CountryModel>.from(
            json["areas"].map((x) => CountryModel.fromJson(x))),
      );
}
